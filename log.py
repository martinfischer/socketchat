"""
log.py - Used to log server events.
"""
from os import path
from datetime import datetime


def log(message, log_server=False, logfile=".socketchatlog"):
    """
    Logs server events, if set to log in the config.
    """
    # if the cfg option log_server is set to yes
    if log_server:
        # home directory
        home = path.expanduser("~")
        # home directory + logfilename
        logfile_path = path.join(home, logfile)
        now = datetime.now()
        timestamp = now.strftime("%Y-%m-%d %H:%M")
        message = "{}  -->  {}\n".format(timestamp, message)
        with open(logfile_path, "a") as f:
            f.write(message)