"""
socketchat_client.py - The socketchat client.
"""
import sys
import socket
import select
import configparser
from crypt import crypt


def prompt(username):
    """
    Prints the user prompt.
    """
    print("<{username}> ".format(username=username), end="", flush=True)


def client():
    """
    The client logic.
    """
    config = configparser.ConfigParser()
    config.read("socketchat_client.cfg")
    # Server host and port according to the config
    aes_key = config["crypt"]["aes_key"]
    host = config["server"]["host"]
    port = int(config["server"]["port"])
    username = config["username"]["username"]
    # Create a socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(2)
    # Try the socket connection to the server
    try :
        sock.connect((host, port))
    except :
        print("Unable to connect.")
        sys.exit()
    # Connection succeeded, so print a prompt
    print("Connected to the socketchat server. Press \"CTRL+C\" to exit the chat.")
    prompt(username)

    while True:
        socket_list = [sys.stdin, sock]
        # Selector for readable sockets
        read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])
        for read_socket in read_sockets:
            # A message is ready to read
            if read_socket == sock:
                data = read_socket.recv(4096)
                # Exit on empty read
                if not data :
                    print("\nDisconnected from the socketchat server.")
                    sys.exit()
                # Message is ready to decrypt
                else :
                    message = crypt(message_to_decrypt=data, aes_key=aes_key)
                    print("\n{}".format(message), end="\n", flush=True)
                    prompt(username)
            # Message is ready to send
            else :
                message = sys.stdin.readline().strip()
                # Do not send empty messages
                if message:
                    message = "<{username}> said: {message}".format(username=username, message=message)
                    message = crypt(message_to_encrypt=message, aes_key=aes_key)
                    sock.send(message)
                prompt(username)


def main():
    try:
        client()
    # Exit on CTRL+C
    except KeyboardInterrupt:
        print("\nManual Exit.")


if __name__ == "__main__":
    main()
