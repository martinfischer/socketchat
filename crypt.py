"""
crypt.py - Used to encrypt and decrypt messages.
"""
from Crypto import Random
from Crypto.Cipher import AES


def crypt(message_to_encrypt=None, message_to_decrypt=None, aes_key=None):
    """
    Encrypts and decrypts messages.

    Only give one keyword per call.

    Keyword arguments:
    message_to_encrypt -- encrypt this message (default None).
    message_to_decrypt -- decrypt this message (default None).
    """
    aes_iv = Random.new().read(AES.block_size)
    bs = AES.block_size
    # pad the message according to block_size
    pad = lambda s: s + (bs - len(s.encode()) % bs) * chr(bs - len(s.encode()) % bs)
    # unpad the allready decrypted message
    unpad = lambda s: s[:-ord(s[len(s)-1:])]

    # if there is a message to encrypt
    if message_to_encrypt:
        message_to_encrypt = pad(message_to_encrypt)
        cypher = AES.new(aes_key, AES.MODE_CBC, aes_iv)
        encrypted_message = aes_iv + cypher.encrypt(message_to_encrypt.encode())
        return encrypted_message

    # if there is a message to decrypt
    if message_to_decrypt:
        aes_key_len = len(aes_key)
        aes_iv = message_to_decrypt[:aes_key_len]
        cypher = AES.new(aes_key, AES.MODE_CBC, aes_iv)
        decrypted_message = unpad(cypher.decrypt(message_to_decrypt[aes_key_len:]))
        return decrypted_message.decode()
