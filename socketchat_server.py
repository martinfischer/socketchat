"""
socketchat_server.py - The socketchat server.
"""
import sys
import asyncio
import argparse
import configparser
from log import log
from crypt import crypt


# Global list of clients that are connected
clients = []

# Command line argumentsa
parser = argparse.ArgumentParser(description="Use a different configfile for the server")
parser.add_argument("-c",
                    "--configfile",
                    metavar="CONFIG-FILEPATH",
                    nargs=1,
                    help="Must be a valid path to a config file",
                    required=False,
                    )
args = parser.parse_args()
if args.configfile:
    configfile = args.configfile[0]
else:
    configfile = "socketchat_server.cfg"

# Read Config
config = configparser.ConfigParser()
config.read(configfile)
decrypt = config["crypt"].getboolean("decrypt", fallback=False)
aes_key = config["crypt"]["aes_key"]
port = int(config["server"]["port"])
log_server = config["log"].getboolean("log_server", fallback=False)
logfile = config["log"]["logfile"]

allowed_client_ips = config["allowed_client_ips"]["ips"]
if allowed_client_ips == "localhost":
    allowed_client_ips = ["127.0.0.1"]
elif allowed_client_ips == "all":
    pass
else:
    allowed_client_ips = [x.strip() for x in allowed_client_ips.split(",")]



class SocketChatServerProtocol(asyncio.Protocol):
    """
    An asyncio.Protocol subclass with methods that override the callbacks.
    """
    def connection_made(self, transport):
        """
        Callback coroutine that is called when a connection has been made by a client.

        Arguments:
        transport -- the asyncio.Transport instance of the caller.
        """
        self.transport = transport
        self.peername = transport.get_extra_info("peername")
        self.ip = self.peername[0]

        # IP is allowed or all IPs are allowed
        if self.ip in allowed_client_ips or allowed_client_ips == "all":
            print("Connection made from {}".format(self.peername))
            log("Connection made from {}".format(self.peername), log_server=log_server, logfile=logfile)
            clients.append(self)
            # If there is more than 1 client connected
            if len(clients) > 1:
                for client in clients:
                    # Send a message to clients allready connected
                    if client is not self:
                        message = "Another client connected: {}".format(self.ip)
                        client.transport.write(crypt(message_to_encrypt=message, aes_key=aes_key))
                    # Send a message to a newly connected client with info about connected clients
                    else:
                        other_clients =[]
                        for client in clients:
                            if client is not self:
                                other_clients.append(client.transport.get_extra_info("peername")[0])
                        message = "Clients already connected: {}".format(", ".join(other_clients))
                        self.transport.write(crypt(message_to_encrypt=message, aes_key=aes_key))
            # If there is only the callers client connected
            elif len(clients) == 1:
                message = "No other clients connected yet."
                self.transport.write(crypt(message_to_encrypt=message, aes_key=aes_key))
        # IP is not allowed, so close the socket
        elif self.ip not in allowed_client_ips and allowed_client_ips != "all":
            self.transport.close()

    def data_received(self, data):
        """
        Callback coroutine that is called when data has been received.

        Arguments:
        data -- The received message.
        """
        if decrypt:
            data = crypt(message_to_decrypt=data, aes_key=aes_key)
            print("{} {}".format(self.peername, data))
            log("{} {}".format(self.peername, data), log_server=log_server, logfile=logfile)
        # Send the message to all clients except the sender
        for client in clients:
            if client is not self:
                if decrypt:
                    message = "{}".format(data)
                    client.transport.write(crypt(message_to_encrypt=message, aes_key=aes_key))
                else:
                    client.transport.write(data)

    def connection_lost(self, ex):
        """
        Callback coroutine that is called when a client disconnected or lost the connection.

        Arguments:
        ex -- The exeption.
        """
        if self.ip not in allowed_client_ips and allowed_client_ips != "all":
            print("Connection rejected: {}".format(self.peername))
            log("Connection rejected: {}".format(self.peername), log_server=log_server, logfile=logfile)
        else:
            print("Connection lost: {}".format(self.peername))
            log("Connection lost: {}".format(self.peername), log_server=log_server, logfile=logfile)
        if self in clients:
            clients.remove(self)
        # Send a message to all still connected clients with the disconnect info
        for client in clients:
            message = "A client disconnected: {}".format(self.ip)
            client.transport.write(crypt(message_to_encrypt=message, aes_key=aes_key))


def main():
    """
    Sets up the eventloop, exits the program on CTRL+C and closes the server afterwards.
    """
    # The event loop
    loop = asyncio.get_event_loop()
    # Create and then run the server
    coro = loop.create_server(SocketChatServerProtocol, "0.0.0.0", port)
    server = loop.run_until_complete(coro)
    # Print server info
    sockname = server.sockets[0].getsockname()
    print("Serving on {}. Press \"CTRL+C\" to exit the server.".format(sockname))
    log("Serving on {}.".format(sockname), log_server=log_server, logfile=logfile)
    # Try to run the loop forever
    try:
        loop.run_forever()
    # Exit on CTRL+C
    except KeyboardInterrupt:
        print("\nManual Exit.")
        log("Manual Exit.", log_server=log_server, logfile=logfile)
    # Close the server
    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()


if __name__ == "__main__":
    main()
