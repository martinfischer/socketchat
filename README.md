# socketchat

*socketchat* is a chatserver and chatclient pair.

Messages are sent with AES encryption.

The clients communicate with the server only, the server sends messages to all connected clients.

Client IPs can be whitelisted in the config.

## Requirements

- python 3.4 (asyncio)
- python3-virtualenv

## Install How To

```
mkdir workspace
cd workspace
virtualenv -p python3 socketchat
cd socketchat
source bin/activate
git clone https://github.com/martinfischer/socketchat.git
cd socketchat
pip install -r requirements.txt
```

## Server Config

Make a copy of `sample-socketchat_server.cfg` and rename it to `socketchat_server.cfg`.

Then edit `socketchat_server.cfg` to your liking.

### [crypt]

#### decrypt:

Decrypt and print to server console, or just send the encrypted message on to the clients. Allowed
values are: yes or no.

*Default: no*

#### aes_key:

Optional when decrypt is set to no. Change this to something random. The key size is 16, if your key
is longer, then the program will cut off the rest. Needs to be the same for all connected peers.

*Default: This is a secret*

### [server]

#### port:

This is the port all communication is running through. Forward that to your server if you connect
from the outside.

*Default: 44444*

### [log]

#### log_server:

Log connects, disconnects and message including client IPs. Allowed values are: yes or no.

*Default: no*

#### logfile:

Choose a filename. The file will be created in your home directory.

*Default: .socketchatlog*

### [allowed_client_ips]

#### ips:

List of client IPs that are allowed to connect, if this option is set to "all" then all IPs are
allowed. Allowed values are: all, localhost or valid IPv4 IP addresses.

*Default: localhost*

## Client Config

Make a copy of `sample-socketchat_client.cfg` and rename it to `socketchat_client.cfg`.

Then edit `socketchat_client.cfg` to your liking.

### [crypt]

#### aes_key:

Change this to something random. The key size is 16, if your key is longer, then the program will
cut off the rest. Needs to be the same for all connected peers.

*Default: This is a secret*

### [server]

#### host:

The IP or hostname where the server will be running.

*Default: localhost*

#### port:

This is the port all communication is running through. Forward that to your server if you connect
from the outside.

*Default: 44444*

### [username]

#### username:

Your username.

*Default: User*

## Run the server

```
cd workspace/socketchat
source bin/activate
cd socketchat
python socketchat_server.py
```

(`CTRL+C` to stop the server)

### ...or use screen to start the server with socketchatserver.sh:

First `chmod +x ~/workspace/socketchat/socketchat/socketchatserver.sh` to make it executable.

`screen -d -m -S socketchatserver ~/workspace/socketchat/socketchat/socketchatserver.sh`
this will detach the process and the terminal, or ssh connection etc can be closed

`screen -d`
this will list detached screen sessions

`screen -r socketchatserver`
this will reattach the process so server output can be seen

(`screen -X -S socketchatserver quit` to stop the server)


## Run the client

```
cd workspace/socketchat
source bin/activate
cd socketchat
python socketchat_client.py
```

### ...or use a function in your .bashrc, .zshrc etc:

```
function scc(){
    cd ~/workspace/socketchat/;
    source bin/activate;
    cd socketchat;
    python socketchat_client.py;
    deactivate;
    cd;
}
```

## Credits
Martin Fischer

## License
MIT