"""
tests.py - Tests most of the server- and client functionality. Only works with the sample configs!
"""
import unittest
import crypt
import socket
import configparser
from time import sleep
from subprocess import call, getoutput


config = configparser.ConfigParser()
config.read("sample-socketchat_client.cfg")
aes_key = config["crypt"]["aes_key"]
host = config["server"]["host"]
port = int(config["server"]["port"])

starts = "screen -d -m -S testsocketchatserver ~/workspace/socketchat/socketchat/socketchatserver.sh -c ~/workspace/socketchat/socketchat/sample-socketchat_server.cfg"
stops = "screen -X -S testsocketchatserver quit"


class TestCrypt(unittest.TestCase):
    def test_crypt(self):
        original_string = "This is a test message!"
        encrypted_string = crypt.crypt(message_to_encrypt=original_string, aes_key=aes_key)
        decrypted_string = crypt.crypt(message_to_decrypt=encrypted_string, aes_key=aes_key)

        self.assertNotEqual(original_string, encrypted_string)
        self.assertEqual(original_string, decrypted_string)


class TestConnection(unittest.TestCase):
    def test_connection(self):
        try:
            call(starts, shell=True)
            sleep(3)

            sock1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock1.settimeout(2)
            sock1.connect((host, port))
            encrypted_response1 = sock1.recv(4096)

            sock2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock2.settimeout(2)
            sock2.connect((host, port))
            encrypted_response2 = sock2.recv(4096)

            sock1.close()
            sock2.close()

            decrypted_response1 = crypt.crypt(message_to_decrypt=encrypted_response1, aes_key=aes_key)
            decrypted_response2 = crypt.crypt(message_to_decrypt=encrypted_response2, aes_key=aes_key)

            self.assertEqual("No other clients connected yet.", decrypted_response1)
            self.assertEqual("Clients already connected: 127.0.0.1", decrypted_response2)
        finally:
            call(stops, shell=True)


class TestCommunication(unittest.TestCase):
    def test_communication(self):
        try:
            call(starts, shell=True)
            sleep(3)

            sock1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock1.settimeout(2)
            sock1.connect((host, port))

            sock2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock2.settimeout(2)
            sock2.connect((host, port))

            message1 = "This is a test message!"
            encrypted_message1 = crypt.crypt(message_to_encrypt=message1, aes_key=aes_key)
            sock1.send(encrypted_message1)

            encrypted_message2 = sock2.recv(4096)
            decrypted_message2 = crypt.crypt(message_to_decrypt=encrypted_message2, aes_key=aes_key)

            encrypted_message3 = sock2.recv(4096)
            decrypted_message3 = crypt.crypt(message_to_decrypt=encrypted_message3, aes_key=aes_key)

            sock1.close()
            sock2.close()

            self.assertEqual("Clients already connected: 127.0.0.1", decrypted_message2)
            self.assertEqual("This is a test message!", decrypted_message3)
        finally:
            call(stops, shell=True)


if __name__ == '__main__':
    unittest.main()